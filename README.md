# **Aplicativos Móveis/Laboratório de Aplicativos Móveis** #

Repositório das disciplinas AM e LAM do Cefet-MG Unidade Varginha, ano letivo 2019

### Como usar este repositório? ###

Este repositório será o principal canal de distribuição de atividades e materiais.

Seções:

* Material de Aula - Slides e textos auxiliares para aulas expositivas
* Exercícios - Exercícios e práticas diversas
* Tutoriais - Exercícios para as aulas práticas
